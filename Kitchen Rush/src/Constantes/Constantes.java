/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constantes;

/**
 *
 * @author User
 * 
 */
public class Constantes {
    
    public static final String RUTA_IMAGENES="/Recursos/imagenes";
    public static final String RUTA_MUSICAS="/Recursos/musicas";
    public static final String RUTA_GAMEDATARECETAS="src/Recursos/datos/gameDataRecetas.dat";
    public static final String RUTA_GAMEDATAINGREDIENTES="src/Recursos/datos/gameDataIngredientes.dat";
    public static final String RUTA_INGREDIENTES="/Recursos/imagenes/Ingredientes";
    public static final String RUTA_AVATARES="src/Recursos/Avatares";
    public static double inicio_ancho = 700;
    public static double inicio_alto = 700;
    public static double juego_ancho = 1280;
    public static double juego_alto = 720;
    public static int tiempo_receta = 30;
    public static int cantidad_recetas = 4;
    public static int tamano_jugador = 58;
    public static int tamano_estacion = 160;
    public static int duracion_partida = 120;
    
}