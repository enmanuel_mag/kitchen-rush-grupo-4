/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;
import Constantes.Constantes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
/**
 *
 * @author Josue
 */
public class Estacion implements Escenario{
    
    
    private ArrayList<Receta> recetasCumplidas;
    private int nCumplidas;
    private ImageView estacion;
    private Jugador player;
    public int tiempo = Constantes.tiempo_receta;
    public int numMesa;
    private Receta recetaActual;
    private Random gn = new Random();
    private HashSet<Ingrediente> ingreActuales = new HashSet<>();
    private HashSet<Ingrediente> ingreDepo = new HashSet<>();
    
    
    
    
    public static ArrayList<Point2D> posiciones = new ArrayList<>(Arrays.asList
        (new Point2D(0,60), new Point2D(Constantes.juego_ancho-160,60)));
    
    public static ArrayList<Point2D> posIngredientes = new ArrayList<>(Arrays.asList
        (new Point2D(0,40+20), new Point2D(50,90+20), new Point2D(100,140+20), new Point2D(0,140+20), new Point2D(100,40+20),
         new Point2D(1250,40+20), new Point2D(1180,90+20), new Point2D(1130,150-10+20), new Point2D(1130,40+20), new Point2D(1230,140+20)));
    
    /**
     * Constructor de la clase Estacion que recibe el numero de mesa la cual depende del jugador. Inicializa las recetas cumplidas, el numero y estacion.
     * @param mesa 
     */
    public Estacion(int mesa){
    
        this.numMesa = mesa;
        
        recetasCumplidas=new ArrayList<Receta>();
        nCumplidas=0;
        Image img = new Image(getClass().getResourceAsStream(
            Constantes.RUTA_IMAGENES+"/estacion.png"), 
            Constantes.tamano_estacion,Constantes.tamano_estacion,true, true);
        estacion = new ImageView(img);
        
        
     
    }
    /**
     * Metodo que devuelve un String que contiene cada una de las recetas cumplidas de la estacion 
     * @return 
     */
    @Override
    public String toString(){
        String cadena="";
        for(Receta rc:recetasCumplidas){
            cadena+=rc.toString()+"\n";     
        }
        return cadena;
    }
    /**
     * Metodo que devuelve un bool dependiendo de que si el numero de ingredientes de la receta actual es igual al numero 
     * de ingredientes que deposita el jugador 
     * @return 
     */
    public boolean completo(){
        if(this.ingreActuales.size() == this.ingreDepo.size()){
            return true;
            }
        return false;
    }
    /**
     * Metodo que fija la estacion en las coordenadas dadas como parametro 
     * @param x
     * @param y 
     */
    
    public void fijarPosicionObjeto(double x, double y){
        
        estacion.setLayoutX(x);
        estacion.setLayoutY(y);
    }
   /**
    * Metodo que fija los nodos de los ingredientes de la receta pasada como parametro en la estacion 
    * @param receta 
    */
    public void mostarIngredientes(Receta receta){
        
        recetaActual=receta; //NUEVO
        ingreActuales = new HashSet<>();
        if(numMesa==1){
            int i=0;
            for(Ingrediente ingrediente : receta.getIngredientes()){
                Ingrediente ingreCopia = Ingrediente.crearCopia(ingrediente);
                ingreActuales.add(ingreCopia);
                
                PanelJuego.gamePane.getChildren().add(ingreCopia.getObjeto()); 
                double x = posIngredientes.get(i).getX(); double y = posIngredientes.get(i).getY();
                ingreCopia.fijarPosicionObjeto(x, y); 
                i++;    
                } 
            }   
        else{
            int a=5;
            for(Ingrediente ingrediente : receta.getIngredientes()){
                Ingrediente ingreCopia = Ingrediente.crearCopia(ingrediente);
                ingreActuales.add(ingreCopia);
                
                PanelJuego.gamePane.getChildren().add(ingreCopia.getObjeto());
                ingreCopia.fijarPosicionObjeto(posIngredientes.get(a).getX(), posIngredientes.get(a).getY()); 
                a++;    
                }  
            }
        }
   /**
    * Metodo que quita el nodo del ingrediente pasado como parametro del nodo estacion.
    * @param i 
    */ 
    public void quitarIngrediente(Ingrediente i){
        
        for(Ingrediente ingrediente : ingreActuales){
            if(i.getNombre().equals(ingrediente.getNombre())){
                
                PanelJuego.gamePane.getChildren().remove(ingrediente.getObjeto());
                }
            }
        }
    
    /**
     * Metodo que agrega el ingrediente pasado como parametro al HashSet que contiene los ingredientes depositados.
     * @param i 
     */
    public void agregaringreidente(Ingrediente i){
        
        ingreDepo.add(i);
        
        
        }
    
    /**
     * Metodo que agrega la receta pasada como parametro al arraylist que contiene a las recetas cumplidas
     * @param receta 
     */
    public void anadirRecetasC(Receta receta){
        
        this.recetasCumplidas.add(receta);   
    }
   

    public ArrayList<Receta> getRecetasCumplidas() {
        return recetasCumplidas;
    }

    public int getnCumplidas() {
        return nCumplidas;
    }

    public int getNumMesa() {
        return numMesa;
    }

    public Receta getRecetaActual() {
        return recetaActual;
    }

    public HashSet<Ingrediente> getIngreActuales() {
        return ingreActuales;
    }

    public HashSet<Ingrediente> getIngreDepo() {
        return ingreDepo;
    }
    
    
    

    public ImageView getObjeto() {
        return estacion;
    }

    public void setIngreActuales(HashSet<Ingrediente> ingreActuales) {
        this.ingreActuales = ingreActuales;
    }
    
    public void setIngreDepo(HashSet<Ingrediente> ingreDepo) {
        this.ingreDepo = ingreDepo;
    }

    
    
    
  
}
