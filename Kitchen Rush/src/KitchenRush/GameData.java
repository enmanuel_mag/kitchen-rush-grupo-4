/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import Constantes.Constantes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Josue
 */
public class GameData {
    /**
     * Metodo estatico que recibe un arreglo de Receta y la escribe en un archivo binario
     * @param recetas
     * @throws IOException 
     */
    public static void guardarRecetas(ArrayList<Receta>recetas) throws IOException{
        
        try{
        ObjectOutputStream objOutputStream=
                new ObjectOutputStream(new FileOutputStream(Constantes.RUTA_GAMEDATARECETAS));
        objOutputStream.writeObject(recetas);
        }catch(IOException exx){
            System.out.println("1");
            System.err.println(exx.getMessage());
            System.err.println(exx.getCause());
            System.err.println(exx.getLocalizedMessage());
            System.err.println(exx.toString());
            throw new IOException();
        }
    }
    /**
     * Metodo estatico que lee un archivo binario y retorna un arreglo de Receta
     * return obj
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static ArrayList<Receta> cargarRecetas() throws IOException,ClassNotFoundException{
        
        try{
        ObjectInputStream objInputStream=
                new ObjectInputStream(new FileInputStream(Constantes.RUTA_GAMEDATARECETAS));
        ArrayList<Receta> obj=(ArrayList<Receta>) objInputStream.readObject();
        return obj;
        }catch(IOException ex2){
            System.out.println("2....");
            throw new IOException();
        }catch(ClassNotFoundException ex3){
            System.out.println("2.1");
            throw new ClassNotFoundException();
        }
    }
    /**
     * Metodo estatico que recibe un arreglo de Ingrediente y lo escribe en un archivo binario
     * @param ingredientes
     * @throws IOException 
     */
    public static void guardarIngredientes(ArrayList<Ingrediente>ingredientes) throws IOException{
        
        try{
        ObjectOutputStream objOutputStream=
                new ObjectOutputStream(new FileOutputStream(Constantes.RUTA_GAMEDATAINGREDIENTES));
        objOutputStream.writeObject(ingredientes);
        
        }catch(IOException exx){
            System.out.println("3");
            System.out.println(exx.getMessage());
            System.out.println(exx);
            throw new IOException();
        }
    }
    /**
     * Metodo estatico que leer un archivo binario y retorna un arreglo de ingredientes
     * @return
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static ArrayList<Ingrediente> cargarIngredientes() throws IOException,ClassNotFoundException{
        
        try{
        ObjectInputStream objInputStream=
                new ObjectInputStream(new FileInputStream(Constantes.RUTA_GAMEDATAINGREDIENTES));
        ArrayList<Ingrediente> obj=(ArrayList<Ingrediente>)objInputStream.readObject();
        return obj;
        }catch(IOException ex2){
            System.out.println("4");
            System.out.println(ex2.getMessage());
            ex2.printStackTrace();
            throw new IOException();
        }catch(ClassNotFoundException ex3){
            System.out.println("4.1");
            throw new ClassNotFoundException();
        }
    }
    /**
     * Metodo auxiliar que llama al metodo estatico guardarIngredientes para guardar un nuevo arreglo
     * de ingredientes
     * @throws IOException 
     */
    public void quemarIngredientes() throws IOException{
        
        ArrayList<String>nIngredientes=new ArrayList<>(Arrays.asList("aguacate","arroz","camaron","cangrejo",
                "carne","cebolla","cereza","choclo","chuleta","frejol","huevo","leche","lechuga","limon",
                "manzana","melon","pan","papa","pasta","pepino","pescado","pina","pollo","queso","tomate","vino","zanahoria"));
        ArrayList<Ingrediente> iQuemados=new ArrayList<>();
        for(String nombre:nIngredientes){
            
            String ruta="/"+nombre+".png";
            Ingrediente ingrediente=new Ingrediente(nombre,ruta);
            iQuemados.add(ingrediente);
            //System.out.println(ruta);
        }
        GameData.guardarIngredientes(iQuemados);
    }
    /**
     * Metodo estatico que recibe el nombre de un ingrediente, lo busca en la base de datos del juego
     * y retorna al ingrediente en caso de que exista, caso contrario retorna null
     * @param nombre
     * @return iQuemados
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static Ingrediente buscarIngrediente(String nombre) throws IOException, ClassNotFoundException{
        ArrayList<Ingrediente> iQuemados=cargarIngredientes();
        Ingrediente i=null;
        for(Ingrediente n:iQuemados){
            if(nombre.equals(n.getNombre()))
                i=n;
        }
        return i;
        
    }
    /**
     * Metodo estatico que recibe un objeto Receta, lo busca en la base de datos del juego y lo retorna
     * en caso de existir, caso contrario retorna null.
     * @param receta
     * @return rQuemados
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static Receta buscarReceta(Receta receta) throws IOException, ClassNotFoundException{
        ArrayList<Receta> rQuemados=cargarRecetas();
        Receta r=null;
        for(Receta n:rQuemados){
            if(n.equals(receta))
                r=n;
        }
        return r;
        
    }
    /**
     * Metodo auxiliar que guarda recetas precargadas en la base de datos del juego
     * @throws IOException 
     */
    public void quemarRecetas() throws IOException{
        ArrayList<Receta>recetas=new ArrayList<>();
        //Haciendo un taco
        try{
        Ingrediente i1=GameData.buscarIngrediente("frejol");
        Ingrediente i2=GameData.buscarIngrediente("carne");
        Ingrediente i3=GameData.buscarIngrediente("cebolla");
        HashSet<Ingrediente> ingredientes=new HashSet<>();
        ingredientes.add(i1);
        ingredientes.add(i2);
        ingredientes.add(i3);
        Receta r1=new Receta("Taco",ingredientes);
        recetas.add(r1);
        }catch(ClassNotFoundException exx){
            System.err.println("Error quemar recetas");
            System.err.println(exx.getCause());
            System.err.println(exx.getMessage());
        }
        try{
        Ingrediente i1=GameData.buscarIngrediente("pescado");
        Ingrediente i2=GameData.buscarIngrediente("limon");
        Ingrediente i3=GameData.buscarIngrediente("cebolla");
        HashSet<Ingrediente> ingredientes=new HashSet<>();
        ingredientes.add(i1);
        ingredientes.add(i2);
        ingredientes.add(i3);
        Receta r2=new Receta("Ceviche de Pescado",ingredientes);
        recetas.add(r2);
        }catch(ClassNotFoundException exx){
            System.err.println("Error quemar recetas");
            System.err.println(exx.getCause());
            System.err.println(exx.getMessage());
        }
        try{
        Ingrediente i1=GameData.buscarIngrediente("arroz");
        Ingrediente i2=GameData.buscarIngrediente("leche");
        Ingrediente i3=GameData.buscarIngrediente("papa");
        Ingrediente i4=GameData.buscarIngrediente("pollo");
        HashSet<Ingrediente> ingredientes=new HashSet<>();
        ingredientes.add(i1);
        ingredientes.add(i2);
        ingredientes.add(i3);
        ingredientes.add(i4);
        Receta r3=new Receta("Pollo con pure de papas",ingredientes);
        recetas.add(r3);
        }catch(ClassNotFoundException exx){
            System.err.println("Error quemar recetas");
            System.err.println(exx.getCause());
            System.err.println(exx.getMessage());
        }
        GameData.guardarRecetas(recetas);
    }
    /**
     * Metodo estatico que recibe un objeto Ingrediente y lo guarda en la base de datos del juego
     * @param ingrediente
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void guardarIngrediente(Ingrediente ingrediente) throws IOException,ClassNotFoundException {
        
        ArrayList<Ingrediente>ingredientesBase=cargarIngredientes();
        ingredientesBase.add(ingrediente);
        guardarIngredientes(ingredientesBase);
        
    }
    /**
     * Metodo estatico que recibe un objeto Receta y lo guarda en la base de datos del juego.
     * @param receta
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static void guardarReceta(Receta receta) throws IOException,ClassNotFoundException{
        ArrayList<Receta> recetasBase=cargarRecetas();
        recetasBase.add(receta);
        guardarRecetas(recetasBase);
        
    }
}
