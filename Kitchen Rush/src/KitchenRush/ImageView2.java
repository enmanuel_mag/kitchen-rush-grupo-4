/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import java.io.Serializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Josue
 */
public class ImageView2 extends ImageView implements Serializable{
    /**
     * Constructor de clase auxiliar, solo se usa de manera pasiva para que no se lance una exception de tipo NotSerializableException
     * @param img 
     */
    public ImageView2(Image img){
        super(img);
    }
}
