/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;
import Constantes.Constantes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
/**
 *
 * @author Josue
 */
public class Ingrediente implements Serializable, Escenario {
    
    private String nombre;
    transient ImageView imagen;
    private String  ruta;
    public static int cont = 0;
    public int ident = 0;
    public Image img;
    
    
    public static ArrayList<Point2D> posiciones = new ArrayList<>(Arrays.asList(
            new Point2D(220,220), new Point2D(220,560),     new Point2D(950,560), new Point2D(950,220),     new Point2D(270,220), new Point2D(270,560),

            new Point2D(1000,560), new Point2D(1000,220),   new Point2D(590,560), new Point2D(590,220),     new Point2D(220,390), new Point2D(950,390),

            new Point2D(640,220), new Point2D(640,560),     new Point2D(270,390), new Point2D(1000,390),     new Point2D(590,390), new Point2D(640,390)));
            
    
    public Ingrediente(){
    }
    /**
     * Constructor de Ingrediente
     * @param nombre
     * @param ruta 
     */
    public Ingrediente(String nombre,String ruta){
        this.nombre=nombre; this.ruta=ruta;
        //setImagen(); 
    }
    /**
     * Constructor de ingrediente
     * @param nombre
     * @param ruta
     * @param copia 
     */
    public Ingrediente(String nombre,String ruta, int copia){
        this.nombre=nombre; this.ruta=ruta; this.ident = copia;
        //setImagen(); 
    }
    
    /**
     * Metodo pasivo que fija las posiciones del ImageView segun los parametros
     * @param x
     * @param y 
     */
    public void fijarPosicionObjeto(double x, double y){
        
        imagen.setLayoutX(x);
        imagen.setLayoutY(y);
    }
    /**
     * Copy Constructor, recibe un ingrediente y retorna uno nuevo con la misma información
     * @param ing
     * @return copia
     */
    public static Ingrediente crearCopia(Ingrediente ing){
        Ingrediente.cont++;
        int i = Ingrediente.cont;
        Ingrediente copia = new Ingrediente(ing.nombre, ing.ruta, i);
        copia.setImagenCopia();
        
        return copia;
    }
    /**
     * Metodo pasivo que setea el ImageView del ingrediente a una nueva posicion
     * @param i 
     */
    public void actualizarIngrediente(Ingrediente i){
        
        Image img = new Image(getClass().getResourceAsStream(
                        Constantes.RUTA_INGREDIENTES+i.ruta), 30,30,true, true);
        
        
        this.imagen.setImage(img); 
        }
    /**
     * Metodo retorna la ruta de la imagen del Ingrediente
     * @return ruta 
     */
    public String getRuta() {
        return ruta;
    }
    /**
     * Retorna el nombre del ingrediente
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Actualiza el nombre de un ingrediente
     * @param nombre 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
/**
 * Devuelve la imagen como nodo
 * @return imagen
 */
    public Node getObjeto2() {
        setImagen();
        return imagen;
    }
    /**
     * Devuelve la imagen como noto
     * @return image
     */
    public Node getObjeto() {
        
        return imagen;
    }
/**
 * Actualiza la imageview de Ingrediente
 */
    public void setImagen() {
        Image img = new Image(getClass().getResourceAsStream(
                        Constantes.RUTA_INGREDIENTES+ruta), 40,40,true, true);
        this.img = img;
        imagen=new ImageView(img);
        
    }
    /**
     * Actualiza la imageview de Ingrediente
     */
    public void setImagenCopia() {
        Image img = new Image(getClass().getResourceAsStream(
                        Constantes.RUTA_INGREDIENTES+ruta), 35,35,true, true);
        imagen=new ImageView(img);
        
    }
    /**
     * Actualiza la imageview de Ingrediente
     */
    public void setImagenCopia2() {
        Image img = new Image(getClass().getResourceAsStream(
                        Constantes.RUTA_INGREDIENTES+ruta), 45,45,true, true);
        imagen=new ImageView(img);
        
    }
    
/**
 * Retorna un hashCode nuevo para cada direccion de memoria
 * @return hash
 */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.nombre);
        hash = 97 * hash + Objects.hashCode(this.ruta);
        return hash;
    }
    
/**
 * Actualiza la ruta de Ingrediente
 * @param ruta 
 */
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    /**
     * Valida la iguadad de 2 Ingredientes, lanza true si dos ingredientes tienen el mismo nombre
     * retorna falso si no son iguales
     * @param obj
     * @return boolean 
     */
    @Override
    public boolean equals(Object obj){
        
        if(obj!=null){
            Ingrediente ing=(Ingrediente)obj;
            if(nombre.equals(ing.getNombre())){
                return true;
            }
        }
        return false;
    }
    /**
     * Metodo retorna el nombre del Ingrediente
     * @return nombre
     */
    @Override//nuevo nuevo
    public String toString(){
        return nombre;
        
    }
    
    
}
