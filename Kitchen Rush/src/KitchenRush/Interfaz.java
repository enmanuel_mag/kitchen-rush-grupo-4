/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import Constantes.Constantes;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author User
 */
public class Interfaz implements Escenario{
    
    private ImageView barra;
    
    /**
     * Constructor de la clase Intefaz que fija un imageview.
     */
    public Interfaz(){
        Image img = new Image(getClass().getResourceAsStream(
                        Constantes.RUTA_IMAGENES+"/interfaz.png"));
        this.barra = new ImageView(img);
        
    }

    @Override
    public Node getObjeto() {
        return barra;
    }
    
}
