/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import Constantes.Constantes;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author User
 */
public class Jugador implements Comparable<Jugador>, Escenario{
    
    public String nombre;
    
    private int puntos=0;
    private ImageView jugador;
    private int velX;
    private int velY;
    private Ingrediente ingrediente;
    private Estacion estacion;
    private Random gn = new Random();
    public int numeroJ = 0;
    public int animacion = 0;
    public boolean colision = false;
    public String ruta; public static String nJ = "";
    
    
    /**
     * Constructor de Jugador. 
     * Recive la velcoidad en X, en Y, numero de jugador y la estacion que le 
     * corresponde
     * 
     * @param x
     * @param y
     * @param num
     * @param est 
     */
    public Jugador(int x,int y, int num, Estacion est){
        
        ArrayList<String> nombres = new ArrayList<>(Arrays.asList("Kirby", "Mario",
                "Megaman", "Metroid", "Pikachu", "Sonic"));
        
        numeroJ = num; 
        this.estacion = est;
        String avatar;
        if(nJ.equals("")){
            avatar = nombres.get(gn.nextInt(nombres.size()));
            nJ = avatar;
        }
        else{
            avatar = nombres.get(gn.nextInt(nombres.size()));
            while(avatar.equals(nJ)){
                avatar = nombres.get(gn.nextInt(nombres.size()));
            }
        }

        String path = "src/Recursos/Avatares"+"/"+avatar+"/0.png";
        ruta=path;
        String p = new File(path).getAbsolutePath();
        
        nombre = avatar;
        
        System.out.println(p.toString());
        
        File f = new File(p);
        URI u = f.toURI();
        
        Image img = new Image(u.toString(),Constantes.tamano_jugador,Constantes.tamano_jugador,true,true);
        //System.out.println(getClass().getResourceAsStream(Constantes.RUTA_AVATARES+"/"+avatar+"/"+"0.png").toString());
        //Image img = new Image(getClass().getResourceAsStream(Constantes.RUTA_AVATARES+"/"+avatar+"/"+"0.png"));
        //agrega imagen al imageView
        jugador = new ImageView(img);
        velX= x; velY = y;
    }
    
    /**
     * Este metodo recibe el array de jugadores y los distribuye simetricamente 
     * en el panel del juego
     * @param array 
     */
    public static void colocarJugadores(ArrayList<Jugador> array){
        
        double izq = 200; double der = Constantes.juego_ancho-200;
        double longitud = der-izq;
        double pos_inicial = (longitud/array.size())/2;
        double incremento = (longitud/array.size())/2;
        int indice=0;
        System.out.println(incremento);
        for(int i=0;i<(array.size())*2;i++){
            
            if(i%2==0 || i == 0){
                
                System.out.println(indice);
                array.get(indice).getObjeto().setLayoutX(200+pos_inicial-50);
                array.get(indice).getObjeto().setLayoutY(65);
                indice++;
                }
            pos_inicial += incremento; 
        }
        
        
    }
    
    

    /**
     * Comparador entre dos jugaodres, compara respecto a los puntos de los mismos
     * @param j
     * @return 
     */
    @Override
    public int compareTo(Jugador j){
        int ultimo=Integer.compare(puntos,j.getPuntos());
        return (ultimo!=0 ? ultimo:nombre.compareTo(j.getNombre()));
    }
    
    /**
     * Retorna el ImageView de jugador
     * @return 
     */
    public ImageView getJugador() {
        
        return jugador;
    }
    
    /**
     * Retorna el numero de jugador 
     * @return 
     */
    public int getNumero() {
        return numeroJ;
    }
    
    /**
     * Retorna un String cno el nombre del jugador
     * @return 
     */
    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + '}';
    }
    
    /**
     * Genera un HashCode de jugador
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
    
    /**
     * Compara dos jugadores respecto al nombre de los mismos
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jugador other = (Jugador) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    /**
     * Setea la iamgen del jugador
     */
    public void setImagenPorDefecto(){
        String p = new File(ruta).getAbsolutePath();
        
        
        File f = new File(p);
        URI u = f.toURI();
        
        Image img = new Image(u.toString(), 50,50,true, true);
        //System.out.println(getClass().getResourceAsStream(Constantes.RUTA_AVATARES+"/"+avatar+"/"+"0.png").toString());
        //Image img = new Image(getClass().getResourceAsStream(Constantes.RUTA_AVATARES+"/"+avatar+"/"+"0.png"));
        //agrega imagen al imageView
        
        this.jugador=new ImageView(img);
    }
    
    
    /**
     * Retira el ingrediente que tenga el jugador
     */
    public void botarIngrediente (){
        this.ingrediente=null;
    }
    
    /**
     * Recebie un ingrediente y lo setea en su variable
     * @param ingrediente 
     */
    public void setIngrediente(Ingrediente ingrediente){
        this.ingrediente=ingrediente;
    }
    
    /**
     * Fija la posicion del ImageView del jugador
     * @param x
     * @param y 
     */
    public void fijarPosicionObjeto(double x, double y){
        jugador.setLayoutX(x);
        jugador.setLayoutY(y);
    }

    /**
     * Retorna el ingrediente del jugador
     * @return 
     */
    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public Estacion getEstacion() {
        return estacion;
    }

    public String getNombre() {
        return nombre;
    }
    
    public Node getObjeto(){
        return jugador;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getVelX() {
        return velX;
    }

    public int getVelY() {
        return velY;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstacion(Estacion estacion) {
        this.estacion = estacion;
    }

    public void setJugador(ImageView jugador) {
        this.jugador = jugador;
    }

    public void setVelX(int i) {
        this.velX = i;
    }

    public void setVelY(int i) {
        this.velY = i;
    }
    
    
    
    
    
    
}
