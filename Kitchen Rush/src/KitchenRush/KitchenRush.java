
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;


import Constantes.Constantes;
import java.io.File;
import java.net.URI;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *
 * @author User
 */
public class KitchenRush extends Application {

    private PanelInicio panelInicio; 
    
    Musicas sound = new Musicas();

    @Override
    public void start(Stage primaryStage) {

        panelInicio = new PanelInicio(primaryStage, true);

        Scene escena;
        escena = new Scene(panelInicio.getRoot(), Constantes.juego_ancho, Constantes.inicio_alto);

        String path = "src/Estilos/DarkTheme.css";
        String p = new File(path).getAbsolutePath();

        File f = new File(p);
        URI u = f.toURI();

        escena.getStylesheets().add(u.toString());

        primaryStage.setScene(escena);

        primaryStage.setTitle("Kitchen Rush - Inicio");

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
