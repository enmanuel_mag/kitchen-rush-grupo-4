/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import Constantes.Constantes;
import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
/**
 *
 * @author Josue
 */
public class Marcador {
    
    private Jugador jugador1;
    private Jugador jugador2;
    private int puntosJ1;
    private int puntosJ2;
/**
 * Constructor de Marcador
 * @param jugador1
 * @param jugador2 
 */
    public Marcador(Jugador jugador1, Jugador jugador2) {
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
    }
/**
 * Retorna los puntos de jugador
 * @return puntosJ1
 */
    public int getPuntosJ1() {
        return puntosJ1;
    }
/**
 * Actualiza la variable de puntos del Jugador
 * @param puntosJ1 
 */
    public void setPuntosJ1(int puntosJ1) {
        this.puntosJ1 = puntosJ1;
    }
/**
 *Retorna los puntos que tiene Jugador 2
 */
    public int getPuntosJ2() {
        return puntosJ2;
    }
/**
 * Actualiza la variable de puntos del jugador 2
 * @param puntosJ2 
 */
    public void setPuntosJ2(int puntosJ2) {
        this.puntosJ2 = puntosJ2;
    }
/**
 * Retorna a jugador1
 * @return jugador 1;
 */
    public Jugador getJugador1() {
        return jugador1;
    }
/**
 * Retorna a jugador2
 * @return jugador 2 
 */
    public Jugador getJugador2() {
        return jugador2;
    }
    
    
    
    
}
