/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import static KitchenRush.PanelInicio.generar;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author User
 */
public class Musicas {
    
    public static MediaPlayer cancion;
    public static boolean estado = true;
    
    
    /**
     * Constructor. Recibe el nivel de volumen (entre 0 y 1), y el estado para 
     * saber si el usuario activo o desactivo el sonido
     * @param volumen
     * @param estado 
     */
    public static void reproducirMusica(float volumen, boolean estado){

        ArrayList<String> canciones = new ArrayList<>(Arrays.asList("Main_Zelda.mp3", 
                "Race_Kirby.mp3", "Gerudo_Zelda.mp3", "Mario64.mp3", "MarioBros.mp3", 
                "MarioWater.mp3", "Pokemon.mp3", "Smash_Bros.mp3", "SpiderMan.mp3"));
        
        String cancion = canciones.get(generar.nextInt(canciones.size()));
        String path = "src/Recursos/musicas"+"/"+cancion;
        String p = new File(path).getAbsolutePath();
        
        File f = new File(p);
        URI u = f.toURI();

        Media media = new Media(u.toString());
        Musicas.cancion = new MediaPlayer(media);
        Musicas.cancion.setVolume(volumen);
        if(estado) Musicas.cancion.play();
        
        //Loop
        Musicas.cancion.setOnEndOfMedia(new Runnable() {    
        public void run() {
             reproducirMusica(volumen, estado);
            }
        });
    
    }
    
}
