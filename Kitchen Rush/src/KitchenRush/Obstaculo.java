/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import static KitchenRush.PanelJuego.gamePane;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author User
 */
public class Obstaculo implements Escenario{
        
        ImageView iO; int pos; boolean creoI = true;
        List<Point2D> poss = new ArrayList<>(Arrays.asList(new Point2D(380,290), new Point2D(860,290)));
        /**
         * Constructor de la clase Obstaculo que inicializa el imageview del obstaculo y lo posiciona.
         * @param pos 
         */
        public Obstaculo(int pos){
            int n = pos + 1;
            String path = "src/Recursos/imagenes/obstaculo"+n+".png";
                String p = new File(path).getAbsolutePath();
                File f = new File(p); URI u = f.toURI();
                Image img = new Image(u.toString());
            iO = new ImageView(img);
            
            
            System.out.println("Entra");
            
            
            iO.setLayoutX(poss.get(pos).getX()); iO.setLayoutY(poss.get(pos).getY());
        }
        

        

        @Override
        public Node getObjeto() {
           return iO; 
        }
        
        
        
    }
