/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import java.util.Random;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import Constantes.Constantes;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;


/**
 *
 * @author User
 */
public class PanelInicio {
    
    private VBox root;
    private Label titulo;
    private Pane paneInicio;
    //public static Scene escena;
    private MediaPlayer musica; 
    public static Random generar= new Random();
    
    
    /**
     * Recibe el primaryStage y un valor boolean para saber si reproducir o no la musica.
     * Genera los 3 botones, Nueva partida, Recetas, Salir y activar o desactivar sonido
     * @param primaryStage
     * @param valor 
     */
    public PanelInicio(Stage primaryStage, boolean valor) {
        
        root = new VBox();
        
        titulo = new Label("");
        
        root.getChildren().add(titulo);
        titulo.setAlignment(Pos.CENTER);
        
        
        String path = "src/Recursos/fondoInicio.jpg";
        String p = new File(path).getAbsolutePath();
        
        File f = new File(p);
        URI u = f.toURI();
        
        
        root.setStyle("-fx-background-image: url('"+Constantes.RUTA_IMAGENES+"/fondoInicio.png');");
        
        
        crearBotones(primaryStage);
        boolean bEstado = Musicas.estado;
        if(valor) Musicas.reproducirMusica(0.6f, bEstado);
        
        root.setAlignment(Pos.CENTER);
        root.setSpacing(50);
        
    }

    /**
     * Crea los 3 bonotes: Nueva partida, Crear Recetas, Salir y Sonido
     * @param primaryStage 
     */
    public void crearBotones(Stage primaryStage){
        
        VBox opciones =new VBox();
        
        //Boton INCIAR PARTIDA
        Button iniciarPartida=new Button("Iniciar partida nueva");
        
        iniciarPartida.getStylesheets().clear();

        iniciarPartida.setOnAction(new EventHandler<ActionEvent>(){
            
            public void handle(ActionEvent event){  
                Musicas.cancion.stop();
                
                PanelJuego panelJuego=null;  
                try {
                    panelJuego = new PanelJuego(primaryStage, musica);
                } catch (IOException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                primaryStage.getScene().setRoot(panelJuego.getRoot());
                
                } 
            });
        
        //-fx-font-size: 9pt;
        //Boton CREAR RECETA
        Button crearReceta=new Button("Crear nueva receta");
        
        crearReceta.setOnAction(new EventHandler<ActionEvent>(){
            
            public void handle(ActionEvent event){
            
             
                PanelRecetas panelReceta= new PanelRecetas(primaryStage, musica);
            
                primaryStage.getScene().setRoot(panelReceta.getRoot());      
                }
            });
        
        //Boton Desactivar Sonido
        Button apagarSonido = new Button("Sonido");
        
        apagarSonido.setOnAction(new EventHandler<ActionEvent>(){
            
            public void handle(ActionEvent event) {
                
                if(Musicas.estado) {Musicas.estado = false; Musicas.cancion.stop();}
                else{Musicas.estado = true; Musicas.reproducirMusica(0.5f, true);}
                
                }
            });
        
        
        //Boton SALIR
        Button salir=new Button("Salir");
        
        salir.setOnAction( new EventHandler<ActionEvent>(){
            
            public void handle(ActionEvent event){
            System.exit(0);
            } 
        });
        
        opciones.getChildren().addAll(iniciarPartida,crearReceta,salir, apagarSonido);
        opciones.setAlignment(Pos.CENTER);
        opciones.setSpacing(30);
        root.getChildren().add(opciones);
        
    }

    
    
    public VBox getRoot() {
        return root;
    }

    public Pane getPaneInicio() {
        return paneInicio;
    }
 
}
