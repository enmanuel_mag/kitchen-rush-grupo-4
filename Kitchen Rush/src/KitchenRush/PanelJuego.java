/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import java.util.Random;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import Constantes.Constantes;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class PanelJuego {
    
    private StackPane root;
    
    //Marcador
    Marcador marcadorFinal;
    
    //Nodos de Interfaz
    private Label marcadorJ1; private Label marcadorJ2;
    private Label cronometro; private Button volver;
    private int tiempoTrans = Constantes.duracion_partida; private Label regresiva;
    private int contador = 4; private boolean finIntro = false;
    
    //Nodos y Variables de Escenario Juego
    public static Pane gamePane;
    private Button pausa;
    public static boolean gameOver = false;
    private Jugador player1; private Jugador player2;
    HBox infoJ1; HBox infoJ2;
    Label tRecet1; Label tRecet2;
    Label add1; Label add2;
    private Estacion est1; private Estacion est2;
    ArrayList<Jugador> jugadores;
    ArrayList<Estacion> estaciones = new ArrayList<>();
    public static ArrayList<Receta> recetas;
    private ArrayList<Escenario> elementos = new ArrayList<>();
    private ArrayList<KeyCode> tPlayer1 = new ArrayList<>(Arrays.asList(KeyCode.A, KeyCode.W, KeyCode.S, KeyCode.D));
    private Random gn = new Random();
    private boolean PW=false;
    private MediaPlayer musica;
    Label nRecet1; Label nRecet2;
    private Ingrediente ingreActual1; private boolean tieneI1 = false;
    private Ingrediente ingreActual2; private boolean tieneI2 = false;
    
    //Hilos
    private Movimiento1 m1; private Movimiento2 m2;
    private KeyCode[] ingresoTeclas = {KeyCode.K, KeyCode.K};
    private Thread mov1; private Thread mov2;
    private Thread tEst1; private Thread tEst2;
    private Thread tiempo; private Thread hiloRegrecivaInicio;
    private Thread poder; private PowerUp powerHILO;
    private Obstaculo o1;
    private Obstaculo o2;
    
    public int cumplidas;
    private Stage primaryStage;
    
    
    /**
     * Recibe el PrimaryStage (para cambiar de escena) y el MediaPlayer para contrar la musica
     * @param primaryStage
     * @param musica
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public PanelJuego(Stage primaryStage, MediaPlayer musica) throws IOException, ClassNotFoundException {
        Ingrediente ingreActual1 = null; ingreActual2 = null;
        root = new StackPane();
        this.primaryStage = primaryStage;
        this.musica = musica;
        

        player1 = new Jugador(15,15,1, est1); player2 = new Jugador(15,15,2, est2);
        elementos.add(player1); elementos.add(player2);
        
        root.setStyle("-fx-background-image: url('"+Constantes.RUTA_IMAGENES+"/escenario.png');"
                    + "-fx-background-repeat: stretch;"
                    + "-fx-background-size: "+Constantes.juego_ancho+" "+(Constantes.juego_alto)+"; "
                    + "-fx-background-position: center center;");
        crearPane(primaryStage, musica);        
        
    }
    
    
    /**
     * Crea la animacion de entrada, la cuenta regresiva.
     * @param primaryStage
     * @param musica 
     */
    public void incioJuego(Stage primaryStage, MediaPlayer musica){
        
        Animacion intro = new Animacion(1200);
        
        hiloRegrecivaInicio = new Thread(intro);
        hiloRegrecivaInicio.start();
        
        while(finIntro) {hiloRegrecivaInicio.stop(); Musicas.reproducirMusica(0.7f, Musicas.estado); System.out.println("Hola");}    
    }
    
    /**
     * Carga las recetas y selecciona al azar y sin repetir la cantidad especificada en las constante
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public void cargarRecetasPartida() throws IOException, ClassNotFoundException{
        
        ArrayList<Receta> recetasTODAS = GameData.cargarRecetas();
        System.out.println("La cantidad total de recetas BASE DE DATOS es: "+ recetasTODAS.size());
        recetas = new ArrayList<>();
        HashSet<Ingrediente> inggredientesP1 = new HashSet<>();
        int numero = 0;
        int cantidad = 0;
        while(cantidad != Constantes.cantidad_recetas){   
            int i = gn.nextInt(recetasTODAS.size());
            
            Receta recetaSelected = recetasTODAS.get(i);
            
            if(!recetas.contains(recetaSelected) && recetaSelected.getIngredientes().size() > 2){
                System.out.println("Indeice receta agg: " + i);
                recetas.add(recetaSelected);
                cantidad++;
                
                Iterator<Ingrediente> iIngre = recetaSelected.getIngredientes().iterator();
                while(iIngre.hasNext()){
                    numero++;
                    Ingrediente ii = iIngre.next();
                    inggredientesP1.add(ii);
                    System.out.println(ii);
                    }
                } 
            }
        System.out.println("La cantidad total de RECETAS EN PARTIDA es: "+ recetas.size());
        System.out.println("Numero de ingre total en espera: " + numero);
        
        Iterator<Ingrediente> iIngre2 = inggredientesP1.iterator();
            while(iIngre2.hasNext()){
                numero++;
                
                System.out.println(iIngre2.next());
                }
        
        System.out.println("Agregados, sin repetirlos: " +inggredientesP1.size() );
        

        int i= 0;
        for(Ingrediente ing : inggredientesP1){          
            ing.setImagen();
            gamePane.getChildren().add(ing.getObjeto());  
            elementos.add(ing);
            
            ing.fijarPosicionObjeto(Ingrediente.posiciones.get(i).getX(), Ingrediente.posiciones.get(i).getY());
            i++;
            
            } 
    }
    
    /**
     * Crea el pane del juego, fija los jugadores, extrae al azar un numero especificado de rectas,
     * fija sus ingrediente en el paneGame y fija las estaciones de los jugadores
     * @param primaryStage
     * @param musica
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public void crearPane(Stage primaryStage, MediaPlayer musica) throws IOException, ClassNotFoundException{
       
        gamePane = new Pane();
        
        regresiva = new Label(Integer.toString(contador));
        regresiva.setFont(new Font("Segoe UI Semibold",120));
        regresiva.setTextFill(Color.web("#2A3638"));
        regresiva.setOpacity(1);
   
        jugadores = new ArrayList<>(); jugadores.add(player1); jugadores.add(player2);
        Jugador.colocarJugadores(jugadores);

        cargarRecetasPartida();  

        crearMesas();
        crearObstaculos();
        crearInterfaz(primaryStage, musica);
        
        gamePane.getChildren().addAll(player1.getObjeto(), player2.getObjeto(), pausa);  
        
        root.getChildren().add(regresiva);
        incioJuego(primaryStage, musica);
        
        root.getChildren().addAll(gamePane);
        } 
    
    /**
     * Crea los obstaculos en la pantalla
     */
    public void crearObstaculos(){
        o1 = new Obstaculo(0); o2 = new Obstaculo(1);
        elementos.add(o1); elementos.add(o2);
        gamePane.getChildren().addAll(o1.getObjeto(), o2.getObjeto());
    }
    
    /**
     * Crea una barra superior para mostrar el tiempo restante de la receta, lso puntos de cada jugadores y el tiempo 
     * transcurrido en partida
     * @param primaryStage
     * @param musica 
     */
    public void crearInterfaz(Stage primaryStage, MediaPlayer musica){
 
            marcadorJ1 = new Label("Puntos: " + player1.getPuntos());
            marcadorJ1.setTextFill(Color.web("#FFFFFF"));
            
            marcadorJ1.setLayoutX(500); marcadorJ1.setLayoutY(20);
            
            marcadorJ2 = new Label("Puntos: " + player2.getPuntos());
            marcadorJ2.setTextFill(Color.web("#FFFFFF"));
            
            marcadorJ2.setLayoutX(715); marcadorJ2.setLayoutY(20);
            
            cronometro = new Label("120");
            cronometro.setTextFill(Color.web("#FFFFFF"));
            
            cronometro.setLayoutX(630); cronometro.setLayoutY(20);
        
        infoJ1 = new HBox();
            VBox R_J1 = new VBox();    
            add1 = new Label("Mesa de " + player1.getNombre().toUpperCase());
            nRecet1 = new Label(est1.getRecetaActual().getNombre());
            R_J1.setLayoutX(100); R_J1.setLayoutY(13);
            R_J1.getChildren().addAll(add1, nRecet1);
            
            tRecet1 = new Label(Integer.toString(est1.tiempo));
            tRecet1.setFont(new Font(20)); tRecet1.setTextFill(Color.web("#FFFFFF"));
            
        infoJ1.getChildren().addAll(tRecet1);
        
        infoJ1.setSpacing(20);
        //------------------------------//    
        infoJ2 = new HBox();  
            VBox R_J2 = new VBox(); 
            add2 = new Label("Mesa de " + player2.getNombre().toUpperCase()); 
            R_J2.setLayoutX(1280-215); R_J2.setLayoutY(13);
            nRecet2 = new Label(est2.getRecetaActual().getNombre());
            R_J2.getChildren().addAll(add2, nRecet2);
            tRecet2 = new Label(Integer.toString(est2.tiempo));
            tRecet2.setFont(new Font(20)); tRecet2.setTextFill(Color.web("#FFFFFF"));
        infoJ2.getChildren().addAll(tRecet2);
        infoJ2.setSpacing(20);
        
        infoJ1.setAlignment(Pos.TOP_LEFT); infoJ1.setAlignment(Pos.TOP_LEFT);
        infoJ1.setLayoutX(20); infoJ1.setLayoutY(15);
        infoJ2.setLayoutX(Constantes.juego_ancho-40); infoJ2.setLayoutY(15);

        //Barra Superior
        
        Interfaz barra = new Interfaz();
        elementos.add(barra);
        
        
        //Ingredientes
        
        this.ingreActual1 = new Ingrediente("ingre de j1", "/vacio.png");
        ingreActual1.setImagenCopia2();
        ingreActual1.getObjeto().setLayoutX(250);
        ingreActual1.getObjeto().setLayoutY(15);
        
        this.ingreActual2 = new Ingrediente("ingre de j2", "/vacio.png");
        ingreActual2.setImagenCopia2();
        ingreActual2.getObjeto().setLayoutX(1280-280);
        ingreActual2.getObjeto().setLayoutY(15);
 
        
        barra.getObjeto().setLayoutX(0);barra.getObjeto().setLayoutY(0);

        
        gamePane.getChildren().addAll(barra.getObjeto(), infoJ1, infoJ2, ingreActual1.getObjeto(),
        ingreActual2.getObjeto(), marcadorJ1, marcadorJ2, cronometro);
        //ui.getChildren().addAll(uno, dos, tres);

        pausa = new Button("Pausa");    
        pausa.setOnAction(new EventHandler<ActionEvent>(){
            
        @Override
        public void handle(ActionEvent event) {
            //Musicas.cancion.stop();
            if(finIntro){
                if(PW){poder.suspend();}
                mov1.suspend();
                mov2.suspend();
                tiempo.suspend();
                tEst1.suspend();
                tEst2.suspend();
                
                gameOver=true;
            gamePane.setFocusTraversable(false); 
            VBox contenedorPausa=new VBox();
            Button bSalir=new Button("Salir");
            Button bRenaudar=new Button("Renaudar");   
            Button bNPartida=new Button("Partida Nueva");
            
            contenedorPausa.setStyle("-fx-background-image: url('"+Constantes.RUTA_IMAGENES+"/fondoPausa.png');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+320+" "+(360)+"; "
                + "-fx-background-position: center center;");
            
            contenedorPausa.getChildren().addAll(bRenaudar,bNPartida,bSalir);
            contenedorPausa.setAlignment(Pos.CENTER);
            contenedorPausa.setSpacing(20);
            root.getChildren().add(contenedorPausa);
            bSalir.setOnAction(new EventHandler<ActionEvent>(){
                
                @Override
                public void handle(ActionEvent event) {
                Musicas.cancion.stop();
                gameOver=false;
                
                tiempo.stop(); mov1.stop();
                tEst1.stop(); mov2.stop();
                tEst2.stop(); 
                if(PW) poder.stop();
                
                PanelInicio pI = new PanelInicio(primaryStage, true);
                primaryStage.getScene().setRoot(pI.getRoot());
                }});
            
            bRenaudar.setOnAction(new EventHandler<ActionEvent>(){
            
                @Override
                public void handle(ActionEvent event) {
                gameOver=false;
                if(PW){poder.resume(); System.out.println("desperto"); System.out.println(poder.isAlive());} 
                tiempo.resume();
                tEst1.resume();
                tEst2.resume();
                mov1.resume();
                mov2.resume();
                
                
                    System.out.println("TA VIVO?");
                    System.out.println(tiempo.isAlive());
                
                
                root.getChildren().remove(contenedorPausa);
                gamePane.setFocusTraversable(true);
                
                
                }});
            
            bNPartida.setOnAction(new EventHandler<ActionEvent>(){
            
            public void handle(ActionEvent event){  
                Musicas.cancion.stop();
                gameOver=false;
                tiempo.stop();
                tEst1.stop();
                tEst2.stop();
                
                PanelJuego panelJuego=null;  
                try {
                    panelJuego = new PanelJuego(primaryStage, musica);
                } catch (IOException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                primaryStage.getScene().setRoot(panelJuego.getRoot());
                
                } 
            });
            
                }
            }
            });
        
        gamePane.getChildren().addAll(R_J1, R_J2);
        pausa.setLayoutX(850); pausa.setLayoutY(14);
        
    }
    
    /**
     * Asigna el puntaje correspondiete al jugador segun al receta.
     * @param ingreDepo
     * @param j 
     */
    public void puntaje(HashSet<Ingrediente>ingreDepo,Jugador j){
        int num= j.getNumero();
        System.out.println("numero de jugador:"+num);
        switch(num){
            case 1:
            if (ingreDepo.size()==3){
                
            
                System.out.println("30 PUNTOS");
                
                int puntaj=player1.getPuntos()+30;
                player1.setPuntos(puntaj);
                marcadorJ1.setText("Puntos: " + Integer.toString(puntaj));   
        }else if (ingreDepo.size()==4){
                System.out.println("40 PUNTOS");
                System.out.println(marcadorJ1.getText());
                int puntaj=player1.getPuntos()+40;
                player1.setPuntos(puntaj);
                marcadorJ1.setText("Puntos: "+Integer.toString(puntaj));
                
        }else if (ingreDepo.size()==5){
                System.out.println("50 PUNTOS");
                System.out.println(marcadorJ1.getText());
                int puntaj=player1.getPuntos()+50;
                player1.setPuntos(puntaj);
                marcadorJ1.setText("Puntos: "+Integer.toString(puntaj));
                }
            break;
            case 2:
                
            if (ingreDepo.size()==3){
                
            
                System.out.println("30 PUNTOS J2");
                System.out.println(marcadorJ2.getText());
                int puntaj=player2.getPuntos()+30;
                player2.setPuntos(puntaj);
                marcadorJ2.setText("Puntos: "+Integer.toString(puntaj));   
        }else if (ingreDepo.size()==4){
                System.out.println("40 PUNTOS J2");
                System.out.println(marcadorJ2.getText());
                int puntaj=player2.getPuntos()+40;
                player2.setPuntos(puntaj);
                marcadorJ2.setText("Puntos: "+Integer.toString(puntaj));
                
        }else if (ingreDepo.size()==5){
                System.out.println("50 PUNTOS J2");
                System.out.println(marcadorJ2.getText());
                int puntaj=player2.getPuntos()+50;
                player2.setPuntos(puntaj);
                marcadorJ2.setText("Puntos: "+Integer.toString(puntaj));
                break;       
        }          
}
    }
      
    /**
     * Crea las estaciones y fija los ingredientes en la estacion
     */
    public void crearMesas(){
        
        est1 = new Estacion(1);
        est2 = new Estacion(2);
        player1.setEstacion(est1);
        player2.setEstacion(est2);
        elementos.add(est1); elementos.add(est2);
        estaciones.add(est1); estaciones.add(est2);
        gamePane.getChildren().addAll(est1.getObjeto(), est2.getObjeto()); 
        est1.fijarPosicionObjeto(Estacion.posiciones.get(0).getX(), Estacion.posiciones.get(0).getY());
        est2.fijarPosicionObjeto(Estacion.posiciones.get(1).getX(), Estacion.posiciones.get(1).getY());
        
        est1.mostarIngredientes(recetas.get(gn.nextInt(recetas.size())));
        est2.mostarIngredientes(recetas.get(gn.nextInt(recetas.size())));    
    }
    
    /**
     * Cambia las iamgenes del jugador para dar la impresion de animacion
     * @param player
     * @param side 
     */
    public void animar(Jugador player, boolean side){
        
        String avatar = player.nombre; 
        int animacion = player.animacion;
        
        String path;
        if(side) path = "src/Recursos/Avatares"+"/"+avatar+"/"+animacion+".png";
        else {path = "src/Recursos/Avatares"+"/"+avatar+"/"+animacion+"flipped.png";} 
        
        String p = new File(path).getAbsolutePath();
        File f = new File(p); URI u = f.toURI();
        Image img = new Image(u.toString(), Constantes.tamano_jugador,Constantes.tamano_jugador,true, true);
        player.getJugador().setImage(img);

        player.animacion++; 
        if(player.animacion == 4) player.animacion = 0;
    }
    
    /**
     * Controla el trafico de teclas en un Array para luego mover a los personajes
     * segun al tecla almacenada en el array
     */
    public void registroTeclas(){
        
        gamePane.setFocusTraversable(true);
        
        gamePane.setOnKeyPressed(e -> {
            
            KeyCode tecla = e.getCode();
            if(tPlayer1.contains(tecla)){           
                ingresoTeclas[0] = tecla;
                }
            else{
                ingresoTeclas[1] = tecla;
                }
            });
        
        gamePane.setOnKeyReleased(e -> {

            KeyCode tecla = e.getCode();
            if(tPlayer1.contains(tecla)){           
                ingresoTeclas[0] = KeyCode.K;
                }
            else{
                ingresoTeclas[1] = KeyCode.K;
                }
            });
        
    }
    
    /**
     * Verifica si dos objetos han chocado
     * @param n1
     * @param n2
     * @return boolean 
     */
    public static boolean isCollision(Node n1, Node n2){
        Bounds b1 = n1.getBoundsInParent();
        Bounds b2 = n2.getBoundsInParent();
        if(b1.intersects(b2)){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Itera en los elementos del escenaria para saber si dos objetos han chocado
     * @param player
     * @param teclaP
     * @return 
     */
    public Escenario chequearColisiones(Jugador player, KeyCode teclaP){
        
        for(Escenario esc : elementos){   
            if(!(player.equals(esc)) && isCollision(player.getObjeto(), esc.getObjeto())){ 
                
                retroceder(player, teclaP);
                player.colision = true;
                Busqueda(esc, player);
                return esc;
                } 
            }
        player.colision = false;
        return null;
        } 
    
    /**
     * Recibe el jugador y la tecla que presiono para retrocederlo instantaneamente
     * @param player
     * @param teclaP 
     */
    public void retroceder(Jugador player, KeyCode teclaP){
        
        if(player.numeroJ==1){
            switch(teclaP){
                case A:
                    player.getObjeto().setLayoutX(player.getObjeto().getLayoutX() + player.getVelX());
                    break; 
                case D:
                    player.getObjeto().setLayoutX(player.getObjeto().getLayoutX() - player.getVelX());
                    break;
                case W:
                    player.getObjeto().setLayoutY(player.getObjeto().getLayoutY() + player.getVelY());
                    break;
                case S:
                    player.getObjeto().setLayoutY(player.getObjeto().getLayoutY() - player.getVelY());
                }
            }
        
        else{
            switch(teclaP){
                case LEFT:
                    player.getObjeto().setLayoutX(player.getObjeto().getLayoutX() + player.getVelX());
                    break; 
                case RIGHT:
                    player.getObjeto().setLayoutX(player.getObjeto().getLayoutX() - player.getVelX());
                    break;
                case UP:
                    player.getObjeto().setLayoutY(player.getObjeto().getLayoutY() + player.getVelY());
                    break;
                case DOWN:
                    player.getObjeto().setLayoutY(player.getObjeto().getLayoutY() - player.getVelY());
                }
            }   
        }
    
    /**
     * Intera en lso elementos del escenario y si detecta que choco con Mesa verifica que sea el correcto y no 
     * repetido y lo asiga a la mesa, si es un ingrediente lo setea al jugador
     * @param objeto
     * @param player 
     */
    public void Busqueda (Escenario objeto, Jugador player){
        
        if(objeto instanceof Ingrediente){

            System.out.println("El jugador " + player.numeroJ + " recogio " + (Ingrediente) objeto);
            player.setIngrediente((Ingrediente) objeto);
 
            if(player.numeroJ == 1){
                System.out.println("Aparece ingre 1");
                //ingreActual1 = Ingrediente.crearCopia((Ingrediente) objeto);
                
                ingreActual1.actualizarIngrediente((Ingrediente) objeto);
                ingreActual1.getObjeto().setVisible(true);
                
                tieneI1 = true;
                }
            else{
                System.out.println("Aparece ingre 2");
                ///ingreActual2 = Ingrediente.crearCopia((Ingrediente) objeto);
                
                ingreActual2.actualizarIngrediente((Ingrediente) objeto);
                ingreActual2.getObjeto().setVisible(true);
                
                tieneI2 = true;
                }
            }
        
        else if(objeto instanceof Estacion){
             if(player.numeroJ == 1){
                if(this.tieneI1){ingreActual1.getObjeto().setVisible(false); tieneI1 = false;} 
                }
             else{
                if(this.tieneI2){ingreActual2.getObjeto().setVisible(false); tieneI2 = false;}  
                }
            Estacion est = (Estacion) objeto;
            boolean buscar = true;
            System.out.println("Receta de ahorita: "+est.getRecetaActual());
            Iterator<Ingrediente> iIngrediente = est.getIngreActuales().iterator();
            while(iIngrediente.hasNext() && buscar){
                Ingrediente i = iIngrediente.next();
                
                if((player.getIngrediente() != null) && i.getNombre().equals(player.getIngrediente().getNombre()) 
                        && player.numeroJ == est.getNumMesa()){
                    System.out.println("Se añadió a la mesa del jugador : "+player.numeroJ + " --> "+i);
                    est.quitarIngrediente(i);
                    est.agregaringreidente(i); buscar = false;
                    
                    if(est.completo()){
                        est.anadirRecetasC(est.getRecetaActual());
                        est.tiempo = 0;
                        System.out.println("SE COMPLETO LA RECETA!");
                        //NUEVO RECETAS CUMPLIDAS
                        //est.setnCumplidas(player.getEstacion().getnCumplidas()+1);
                        
                        puntaje(est.getIngreDepo(),player);
                        
                        }
                    }
                }
            player.botarIngrediente();
            System.out.println("Total ingre deposit de la mesa "+player.numeroJ+ ": " +est.getIngreActuales().size() + " Total requeridos: " +est.getIngreDepo().size());
            }
        
        
        
        }
    
    
    /**
     * Hilo que constantemente revisa un indice espeficio en el array de teclas para
     * mover al jugador como corresponde
     */
    public class Movimiento1 implements Runnable{

        @Override
        public void run() {
            
            while(!gameOver){
                
                Platform.runLater(() -> {
                    
                KeyCode tecla = ingresoTeclas[0];
                
                switch (tecla) {
                    case W:  
                        if(!gameOver){
                        double y1 = player1.getObjeto().getLayoutY() - player1.getVelY();
                        player1.getObjeto().setLayoutY(y1);
                            }
                        break;
                    case S: 
                        if(!gameOver){                         
                            double y2 = player1.getObjeto().getLayoutY() + player1.getVelY();
                            player1.getObjeto().setLayoutY(y2);
                            }
                        break;
                        
                    case A: 
                        
                        if(!gameOver){
                            if(!player1.colision) animar(player1, false);
                    
                            double x1 = player1.getObjeto().getLayoutX() - player1.getVelX();
                            player1.getObjeto().setLayoutX(x1);
                            }
                        break;
                    case D: 
                        if(!gameOver){
                            if(!player1.colision) animar(player1, true);
                            double x2 = player1.getObjeto().getLayoutX() + player1.getVelX();
                            player1.getObjeto().setLayoutX(x2);
                            }
                        break;
                    }
                chequearColisiones(player1, tecla);
                
                double posX = player1.getObjeto().getLayoutX() ; double posY = player1.getObjeto().getLayoutY();
                if(posX <= 0 || posX >= 1280-50 || posY >= 720-60){
                    retroceder(player1, tecla);
                    System.out.println("No pasa borde");
                    }
                
                });
                try {
                    Thread.sleep(38);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PanelJuego.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
            }   
        }
    
    /**
     * Hilo que constantemente revisa un indice espeficio en el array de teclas para
     * mover al jugador como corresponde
     */
    public class Movimiento2 implements Runnable{

        @Override
        public void run() {
            
            while(!gameOver){
                
                Platform.runLater(() -> {
                
                KeyCode tecla = ingresoTeclas[1];
                
                switch (tecla) {
                    case UP:  
                        if(!gameOver){
                        
                        double y1 = player2.getObjeto().getLayoutY() - player2.getVelY();
                        player2.getObjeto().setLayoutY(y1);
                            }
                        break;
                    case DOWN: 
                        if(!gameOver){                         
                            double y2 = player2.getObjeto().getLayoutY() + player2.getVelY();
                            player2.getObjeto().setLayoutY(y2);
                            }
                        break;
                        
                    case LEFT: 
                        
                        if(!gameOver){
                            if(!player2.colision) animar(player2, false);
                            double x1 = player2.getObjeto().getLayoutX() - player2.getVelX();
                            player2.getObjeto().setLayoutX(x1);
                            }
                        break;
                    case RIGHT: 
                        if(!gameOver){
                            if(!player2.colision) animar(player2, true);
                            double x2 = player2.getObjeto().getLayoutX() + player2.getVelX();
                            player2.getObjeto().setLayoutX(x2);
                            }
                        break;
                    }
                chequearColisiones(player2, tecla);
                
                double posX = player2.getObjeto().getLayoutX() ; double posY = player2.getObjeto().getLayoutY();
                if(posX <= 0 || posX >= 1280-50 || posY >= 720-60){
                    retroceder(player2, tecla);
                    System.out.println("No pasa borde");
                    }
                
                    });
                try {
                    Thread.sleep(38);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PanelJuego.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                }
            }   
        }
    
    /**
     * Hilo para correr el tiempo del juego
     */
    public class Tiempo implements Runnable{
        
        private Stage primaryStage;
        
        public Tiempo(Stage primaryStage){
            this.primaryStage = primaryStage;
        }
        
        List<Integer> intervalos = new ArrayList<>(Arrays.asList(95, 60, 35, 15));
        List<Point2D> pos = new ArrayList<>(Arrays.asList( new Point2D(660,500), new Point2D(660,140),
                    new Point2D(850,150), new Point2D(400,350)));

        @Override
        public void run() {
            while(!gameOver){
                
                Platform.runLater(() -> {tiempoTrans-= 1; cronometro.setText(Integer.toString(tiempoTrans));});
                
                if(110 ==(tiempoTrans) || 90 == tiempoTrans || 60 == tiempoTrans || 30 == tiempoTrans || 10 == tiempoTrans){
                    
                    int power = 1;
                    int ind = gn.nextInt(pos.size());
                    powerHILO = new PowerUp(power, pos.get(ind).getX(), pos.get(ind).getY());
                    poder = new Thread(powerHILO);
                    poder.start();
                    PW = true;
                    }
                
                if(tiempoTrans<=0){ 
                gameOver = true;
                System.out.println("ESO ES TODO AMIGOS");
                if(gameOver==true){
                
                tEst1.stop();
                tEst2.stop();
                
                player1.setImagenPorDefecto();
                player2.setImagenPorDefecto();
                marcadorFinal= new Marcador(player1,player2);
                marcadorFinal.setPuntosJ1(player1.getPuntos());
                marcadorFinal.setPuntosJ1(player2.getPuntos());
                PanelResultados presul= new PanelResultados(marcadorFinal, primaryStage, musica);
                //root.getChildren().add(presul.getRoot());
                primaryStage.getScene().setRoot(presul.getRoot());
                System.out.println("PUNTOS FINAL: J1---->"+player1.getPuntos());
                System.out.println("PUNTOS FINAL: J2---->"+player2.getPuntos());
            }
                }
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException ex){
                    System.err.println("Error en el cronometro :(");
                }   
            }
            tiempo.stop();
            System.out.println(gameOver);
            
        }    
    }
    
    /**
     * Hilo para realizar la animacion de incio
     */
    public class Animacion implements Runnable{
        
        long duracion;
        
        public Animacion(long duracion){
            this.duracion = duracion;
        }

        @Override
        public void run() {

            while(!finIntro){
                Platform.runLater(()->{contador-= 1; regresiva.setText(Integer.toString(contador));
                if(contador == 1) {

                    regresiva.setText("¡A JUGAR!"); 
                    regresiva.setTextFill(Color.web("#355C7D"));
                    finIntro = true;  
                    }
                });
                
            try {
                Thread.sleep(duracion); 
                }catch (InterruptedException ex) {
                    System.out.println("No se");
                }
            }
            regresiva.setVisible(false);
            Musicas.reproducirMusica(0.6f, Musicas.estado);
            registroTeclas(); 
            
            //Inicializacion de Hilos
            Tiempo t = new Tiempo(primaryStage);
            tiempo = new Thread(t);
            tiempo.start();

            //Hilos de movimiento
            m1 = new Movimiento1(); m2 = new Movimiento2();
            mov1 = new Thread(m1); mov2 = new Thread(m2); 
            mov1.start(); mov2.start();
            
            //Tiempo para cada receta
            TiempoReceta tRect1 = new TiempoReceta(est1, tRecet1);
            TiempoReceta tRect2 = new TiempoReceta(est2, tRecet2);
            tEst1 = new Thread(tRect1); tEst2 = new Thread(tRect2);
            tEst1.start(); tEst2.start();
            
            hiloRegrecivaInicio.stop(); 
        }   
    }
    
    /**
     * Hilo para controlar el tiempo de espera de cada receta, quita y agrega ingredientes respectivamente
     */
    public class TiempoReceta implements Runnable{
        
        private Estacion estacion;
        private Label indi;
        private boolean veces = true;
        private int indAnterior;
        public TiempoReceta(Estacion estm, Label indicador){
            this.estacion = estm; indi = indicador;
        }

        @Override
        public void run() {
            
            while(!PanelJuego.gameOver){
                
                Platform.runLater(()->{
                    
                    estacion.tiempo -= 1;
                    indi.setText(Integer.toString(estacion.tiempo));
                    
                    if(estacion.tiempo <=0){
                        
                        
                        Iterator<Ingrediente> iComplete = estacion.getIngreActuales().iterator();
                        while(iComplete.hasNext()){
                            estacion.quitarIngrediente(iComplete.next());
                            }
                        
                        
                        System.out.println("MESA: " + estacion.numMesa);
                        if(veces){
                            int n = gn.nextInt(recetas.size());
                            veces = false;
                            indAnterior = n;
                            }
                        int n = gn.nextInt(recetas.size());
                        while(indAnterior == n){
                            n = gn.nextInt(recetas.size()); 
                            }
                        Receta nRecet = recetas.get(n);
                        indAnterior = n;
                        System.out.println("Indiece de recta anterior: "+ indAnterior+ " Indeice de nueva receta: " + n);
                        System.out.println("Ingredientes: ");
                        System.out.println(nRecet.getIngredientes());
                        
                        estacion.mostarIngredientes(nRecet);
                        if(estacion.numMesa == 1){
                            nRecet1.setText(nRecet.getNombre());
                            }
                        else{
                            nRecet2.setText(nRecet.getNombre());
                            }
                        
                        System.out.println("Se quito y cambio");
                        estacion.setIngreDepo(new HashSet<>());
                        System.out.println("------");
                        estacion.tiempo = Constantes.tiempo_receta;   
                        }
                    });
                
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException ex){
                    System.err.println("Error en el tiempo de la estacion :(");
                }    
            }             
        }   
    }
    
    /**
     * Hilo que genera un PowerUp y mantiene el poder una vez que un jugador toque el ImageView
     * del PowerUp
     */
    public class PowerUp implements Runnable, Escenario{
        
        
        int tiempo = 0;
        int indicador;
        int animacion;
        int incio;
        int velx;
        int vely;
        //Image img;
        ImageView i;
        double x; double y;
        Jugador playerPoder;
        boolean creo = false;
        
        public PowerUp(int indicador, double x, double y){
            
            String path = "src/Recursos/imagenes/PowerUp/"+animacion+".png";
                String p = new File(path).getAbsolutePath();
                File f = new File(p); URI u = f.toURI();
                Image img = new Image(u.toString());
                i = new ImageView(img);
            
            this.x = x; this.y = y;  this.indicador = indicador;
            if(indicador == 1){
                animacion = 0;
                incio = 0;
                }
            else{
                animacion = 7;
                incio = 0;
            }
        }
        
        public void subirVel(Jugador player){
            this.playerPoder = player;
            this.velx = player.getVelX();
            this.vely = player.getVelY();
            playerPoder.setVelX(playerPoder.getVelX()+12);
            playerPoder.setVelY(playerPoder.getVelY()+12);
            }
        
        public void bajarVel(){
            playerPoder.setVelX(velx);
            playerPoder.setVelY(vely);
            if(playerPoder.numeroJ == 1){
                add1.setText("Mesa de " + player1.getNombre().toUpperCase());
                }
            else{
                add2.setText("Mesa de " + player2.getNombre().toUpperCase());
                }
            }
           
        @Override
        public void run() {
            
            System.out.println("Se mostro POWERUP!");
            while(tiempo != 200){
                
            Platform.runLater(() -> {
                
                if(tiempo == 0){
                    String path = "src/Recursos/imagenes/PowerUp/"+animacion+".png";
                    String p = new File(path).getAbsolutePath();
                    File f = new File(p); URI u = f.toURI();
                    Image img = new Image(u.toString());
                    i = new ImageView(img);
                    gamePane.getChildren().add(i);
                    creo = true;
                    i.setLayoutX(x); i.setLayoutY(y);    
                    }        
                if(PW && creo) {
                    i.setVisible(PW);
                    if(PanelJuego.isCollision(i, player1.getObjeto())){
                        gamePane.getChildren().remove(i); PW = false;
                        subirVel(player1); playerPoder = player1 ; tiempo = 1;
                        add1.setText("¡Aumento de VELOCIDAD!");
                        System.out.println("Aumento J1");
                        }
                    else if(PanelJuego.isCollision(i, player2.getObjeto())){
                        gamePane.getChildren().remove(i); PW = false;
                        subirVel(player2) ;playerPoder = player2; tiempo = 1;   
                        add2.setText("¡Aumento de VELOCIDAD!");
                        System.out.println("Aumento J2");
                    }
                } 
                String path2 = "src/Recursos/imagenes/PowerUp/"+animacion+".png";
                String p2 = new File(path2).getAbsolutePath();
                File f2 = new File(p2); URI u2 = f2.toURI();
                Image img2 = new Image(u2.toString());
                i.setImage(img2);
                
                if(animacion <12){animacion++;}
                else{animacion = incio;}
                
                if(tiempo == 198){
                    gamePane.getChildren().remove(i);
                    PW = false; i.setVisible(PW);
                    System.out.println("Desaparece PowerUp!");
                    if(playerPoder != null) {bajarVel();
                        }
                }
            });
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    System.err.println("Error en PowerUp :(");
                }      
                tiempo++;
            }   
            poder.stop();    
        }
        @Override
        public Node getObjeto() {
            return null;
        }
    }
    
    
  
    
    public StackPane getRoot() {
        return root;
    }

    public Label getMarcadorJ1() {
        return marcadorJ1;
    }

    public Label getMarcadorJ2() {
        return marcadorJ2;
    }

    public Label getCronometro() {
        return cronometro;
    }

    public Jugador getPlayer1() {
        return player1;
    }

    public Jugador getPlayer2() {
        return player2;
    }

    public Pane getGamePane() {
        return gamePane;
    }

    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }    
}