/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;

import java.util.Random;
import static java.nio.file.StandardCopyOption.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import Constantes.Constantes;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.media.Media;

import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.StageStyle;
/**
 *
 * @author Sheyla
 */
public class PanelRecetas {
    private final Font FONT = Font.font("Helvetica", FontWeight.MEDIUM, 18 );
    private BorderPane root; //nuevo
    private Stage primaryStage;
    //private VBox root;
    private VBox top3;
    private int veces = 0;
    private Label nombre_platillo;
    private TextField texto_platillo;
    private Label num_ingrediente;
    private VBox vertical;
    private HBox primerTop;
    //private Desktop escritorio=Desktop.getDesktop();
 /**
  * Constructor de la clase PanelRecetas que recibe el stage y mediaplayer, fija el estilo del nodo raiz y llama a los metodos crearSeccionIngredientes y crearBotones
  * @param primaryStage
  * @param musica 
  */
    public PanelRecetas(Stage primaryStage, MediaPlayer musica){
        this.primaryStage= primaryStage;
        root= new BorderPane(); //en argumento espacio entre children 
        root.setStyle("-fx-background-image: url('"+Constantes.RUTA_IMAGENES+"/fondoRecetas.png');");
        
        crearSeccionIngredientes();
        crearBotones(primaryStage, musica);
      
        
    }
    /**
     * Metodo que genera Label y textBox que piden al usuario el nombre de la receta que desea ingresar
     */
    public void crearSeccionPlatillo(){
       
    primerTop= new HBox();
        nombre_platillo= new Label("Nombre platillo: ");
        nombre_platillo.setTextFill(Color.CADETBLUE);
        nombre_platillo.setFont(FONT);
        texto_platillo= new TextField();
        primerTop.getChildren().addAll(nombre_platillo,texto_platillo);
        primerTop.setSpacing(20);
       
}
    /**
     * Metodo que solicita al usuario en un combobox el numero de ingredientes de la receta y despliega labels y textbox requiriendo la informacion para guardar 
     * la receta.
     */
    public void crearSeccionIngredientes(){
        crearSeccionPlatillo();
        vertical= new VBox();
        top3=new VBox();
        ObservableList<String> options= 
                FXCollections.observableArrayList(
                        "3","4","5"
                );
        
        ComboBox <String> comboOpciones= new ComboBox<>(options);
        
        Label num_ingrediente= new Label("Seleccione numero de ingredientes: ");
        num_ingrediente.setFont(FONT);
        num_ingrediente.setTextFill(Color.CADETBLUE);
        //se crea el top
        HBox segundoTop= new HBox(num_ingrediente,comboOpciones);
        segundoTop.setSpacing(20);
        //root.getChildren().add(segundoTop);
        primerTop.setAlignment(Pos.CENTER);
        segundoTop.setAlignment(Pos.CENTER);
        vertical.getChildren().addAll(primerTop,segundoTop);
        vertical.setSpacing(20);
        vertical.setAlignment(Pos.CENTER);
        root.setTop(vertical);
        
        //accion 
        comboOpciones.setOnAction(new EventHandler<ActionEvent>(){
            @Override 
            
            public void handle(ActionEvent event){
                veces++;
                //top3.getChildren().setAll(new Label(""));
                //limpio top3 para que no se acumulen las cajitas
                //top3.getChildren().clear();
                int i=0;
                int num_ingredientes= Integer.parseInt(comboOpciones.getValue()); 
                if(num_ingredientes>top3.getChildren().size()){
                    num_ingredientes=num_ingredientes-top3.getChildren().size();
                    for (i=0;i<num_ingredientes;i++){
                    VBox contenido=new VBox();//Contenedor de cada casilla de ingrediente
                    Label rutaIngrediente=new Label();//ruta cogida del filechooser
                    
                    ObservableList<Ingrediente> ingredientesCargados=FXCollections.emptyObservableList();
                    try{
                    ingredientesCargados=
                            FXCollections.observableArrayList(GameData.cargarIngredientes());
                    
                    }catch(IOException e){//manejar luego en el constructor
                        pantallaEmergente3();
                    }catch(ClassNotFoundException e2){
                        pantallaEmergente3();
                    }
                    final ComboBox <Ingrediente>escoger=new ComboBox<>(ingredientesCargados);
                    
                    rutaIngrediente.setFont(FONT);//estilo
                    rutaIngrediente.setTextFill(Color.CADETBLUE);//estilo
                    rutaIngrediente.setTranslateX(Constantes.juego_ancho/2.8);
                    
                    Label nombre_ingrediente= new Label("Ingrese nombre del ingrediente: ");//subtitulo
                    nombre_ingrediente.setFont(FONT);
                    nombre_ingrediente.setTextFill(Color.CADETBLUE);
                    nombre_ingrediente.setTranslateX(Constantes.juego_ancho/2.8);
                    
                    
                    TextField ningredienteimp= new TextField();//nombre ingresado por el usuario
                    
                    ningredienteimp.setMaxWidth(370);
                    ningredienteimp.setFont(FONT);
                    ningredienteimp.setTranslateX(Constantes.juego_ancho/2.8);
                    
                    HBox contenedorChecks=new HBox();
                    Button examinar= new Button("Examinar");
                    //examinar.setAlignment(Pos.CENTER);
                    
                    CheckBox seleccionE=new CheckBox("Seleccionar uno ya existente");
                    seleccionE.autosize();
                    seleccionE.setFont(FONT);//estilo
                    seleccionE.setTextFill(Color.CADETBLUE);//estilo
                    //seleccionE.setAlignment(Pos.CENTER);
                    
                    
                    CheckBox seleccionX=new CheckBox("Buscar con examinador");
                    seleccionX.autosize();
                    seleccionX.setFont(FONT);//estilo
                    seleccionX.setTextFill(Color.CADETBLUE);//estilo
                    //seleccionX.setAlignment(Pos.CENTER);
                    
                    contenedorChecks.getChildren().addAll(seleccionE,seleccionX);
                    contenedorChecks.setAlignment(Pos.CENTER);
                    seleccionX.setOnAction(new EventHandler<ActionEvent>(){
                    
                        public void handle(ActionEvent e){
                            if(seleccionE.isSelected()){
                                seleccionE.setSelected(false);
                                contenedorChecks.getChildren().remove(2);
                                rutaIngrediente.setText("");
                                ningredienteimp.setText("");
                            }
                            contenedorChecks.getChildren().add(examinar);
                        }
                        
                    });
                    seleccionE.setOnAction(new EventHandler<ActionEvent>(){
                    
                        public void handle(ActionEvent e){
                            if(seleccionX.isSelected()){
                                seleccionX.setSelected(false);
                                contenedorChecks.getChildren().remove(2);
                                rutaIngrediente.setText("");
                                ningredienteimp.setText("");
                            }
                            contenedorChecks.getChildren().add(escoger);
                        }
                        
                    });
                    
                    escoger.setOnAction(new EventHandler<ActionEvent>(){
                    @Override 
                    public void handle(ActionEvent ae){
                        
                        Ingrediente iselected=escoger.getValue();
                        ningredienteimp.setText(iselected.getNombre());
                        rutaIngrediente.setText(iselected.getRuta());
                        
                    }
                });
                    
                    
                    examinar.setOnAction(new EventHandler<ActionEvent>(){
                    @Override 
                    public void handle(ActionEvent ae){
                        
                        FileChooser fileChooser= new FileChooser();
                        fileChooser.setTitle("Seleccionar archivo");
                        fileChooser.getExtensionFilters().add(
                                new ExtensionFilter("Imagen","*.png"));
                        
                        File file= fileChooser.showOpenDialog(primaryStage);
                        
                        if (file!=null){
                        rutaIngrediente.setText(file.getPath());
                        }else{
                            pantallaEmergente();
                        }
                        
                    }
                });
                    
                    contenido.getChildren().addAll(nombre_ingrediente, ningredienteimp,rutaIngrediente, contenedorChecks);
                    top3.getChildren().add(contenido);
                    }
                    
                }else if(num_ingredientes<top3.getChildren().size()){
                    
                    for(int j=(top3.getChildren().size());j>num_ingredientes;j--){
                        top3.getChildren().remove(j-1);
                    }
                }
                
                if(veces==1){
                    //root.getChildren().add(top3);
                    //top3.setAlignment(Pos.CENTER);
                    root.setCenter(top3);
                    
                    }
                }
            });
            
        }
    
    /**
     * Metodo que recibe el stage y mediplayer. Genera los botones de guardar, limpiar, volver y maneja cada uno de ellos.
     * @param primaryStage
     * @param musica 
     */
    public void crearBotones(Stage primaryStage, MediaPlayer musica){
        HBox cuartoTop= new HBox(20);
        
        Button b_guardar= new Button("Guardar");
        Button b_limpiar= new Button("Limpiar");
        Button b_volver = new Button("Volver"); 
        
        
        //manejando boton de limpiar
        b_limpiar.setOnAction(new EventHandler<ActionEvent>(){
        @Override
        public void handle(ActionEvent ae){
            texto_platillo.clear();
            for(Node caja:top3.getChildren()){
            VBox contenido=(VBox)caja;
                
            TextField nombre=(TextField)(contenido.getChildren().get(1));
            Label ruta=(Label)(contenido.getChildren().get(2));
            HBox contenedorcheck1=(HBox)(contenido.getChildren().get(3));
            CheckBox c1=(CheckBox)contenedorcheck1.getChildren().get(0);
            c1.setSelected(false);
            CheckBox c2=(CheckBox)contenedorcheck1.getChildren().get(1);
            c2.setSelected(false);
            if(contenedorcheck1.getChildren().size()==3){
                contenedorcheck1.getChildren().remove(2);
            }
            nombre.clear();
            ruta.setText("");
            //para setear a cero la lista observable del combobox
            //((ComboBox)((HBox)vertical.getChildren().get(1)).getChildren().get(1)).setValue();
        }
        }
        }
      );
        
        //manejando boton de volver 
        b_volver.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                
                PanelInicio pI = new PanelInicio(primaryStage, false);
                primaryStage.getScene().setRoot(pI.getRoot());
            }
        
        
        });
        
        //manejando boton de guardar
        b_guardar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if (estaLleno()){
                    
                    HashSet<Ingrediente> ingredientes=new HashSet<>();
                    
                    for(Node caja:top3.getChildren()){
                        VBox contenido=(VBox)caja;
                        TextField nombre=(TextField)(contenido.getChildren().get(1));
                        Label ruta=(Label)(contenido.getChildren().get(2));
                        
                        File original = new File(ruta.getText());
                        File destino = new File("src"+Constantes.RUTA_INGREDIENTES+"/"+nombre.getText().toLowerCase()+".png");

                        try{
                            //Se busca si existe ese ingrediente para no copiarlo caso contrario se crea el ingrediente
                            Ingrediente ingredienteNuevo=new Ingrediente();
                            if(GameData.buscarIngrediente(nombre.getText().toLowerCase())==null){
                            //se copia el .png de la imagen nueva en los recursos del proyecto
                            Files.copy(original.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                            System.out.println("Se copio con exito");
                            //se guarda el nuevo ingrediente en los recursos del juego
                            String semirruta="/"+nombre.getText().toLowerCase()+".png";//creamos la semirruta
                            ingredienteNuevo.setNombre(nombre.getText().toLowerCase());
                            ingredienteNuevo.setRuta(semirruta);
                            GameData.guardarIngrediente(ingredienteNuevo);
                            System.out.println("Se guardo el ingrediente con exito");
                            }else{
                                ingredienteNuevo=(Ingrediente)GameData.buscarIngrediente(nombre.getText().toLowerCase());
                            }
                            ingredientes.add(ingredienteNuevo);
                            
                            }catch(IOException e1){
                                pantallaEmergente3();
                            }catch(ClassNotFoundException e2){
                                System.err.println("Error al leer ingredientes");
                            }
                    }
                    Receta recetaNueva=new Receta(texto_platillo.getText(),ingredientes);
                    //valido que la receta no exista 
                    try{
                        if(GameData.buscarReceta(recetaNueva)==null){
                            GameData.guardarReceta(recetaNueva);
                            System.out.println("Se ha guardado correctamente la receta");
                            pantallaEmergente5();
                        }else{
                            pantallaEmergente4();
                        }
                        
                    }catch(IOException e1){
                        e1.printStackTrace();
                        System.err.println("Error al abrir el archivo de recetas");;
                    }catch(ClassNotFoundException e2){
                        System.err.println("Error al leer recetas");
                    }
                }else{
                    pantallaEmergente2(); //blancos vacios 
                }
            }
        });
        
        cuartoTop.getChildren().addAll(b_guardar,b_limpiar,b_volver);
        //root.getChildren().add(cuartoTop);
        root.setBottom(cuartoTop);
        cuartoTop.setAlignment(Pos.CENTER);    
    }
/**
 * Metodo que genera una pantalla emergente cada vez que se ingresa una imagen de un ingrediente cuyo tipo no es .png 
 */
    public void pantallaEmergente(){//ventana salta cuando no se escoge una imagen png
        
        Alert pemergente= new Alert(AlertType.INFORMATION); //tipo de alerta
        pemergente.setTitle("Archivo ingresado");
        pemergente.setHeaderText(null);
        pemergente.setContentText("Por favor si abre el examinador, escoja un archivo gracias"); //mensaje
        //pemergente.initStyle(StageStyle.UTILITY);
        pemergente.initStyle(StageStyle.UTILITY); //estilo
        pemergente.showAndWait();
    
}
    /**
     * Metodo que genera una pantalla emergente cada vez que el usuario no haya llenado todos los campos solicitados 
     */
    public void pantallaEmergente2(){//ventana salta cuando no se han llenado los campos
        Alert pemergente= new Alert(AlertType.INFORMATION); //tipo de alerta
        pemergente.setTitle("Error de llenado");
        pemergente.setHeaderText(null);
        pemergente.setContentText("Por favor ingrese el numero de ingredientes solicitados"); //mensaje
        //pemergente.initStyle(StageStyle.UTILITY); //estilo
        
        pemergente.initStyle(primaryStage.getStyle()); //estilo
        pemergente.showAndWait();
    }
    /**
     * Metodo que genera una pantalla emergente cada vez que se arroje una excepcion al guardar los datos 
     */
    public void pantallaEmergente3(){//ventana salta cuado se arroja una excepcion cuando se guardan datos
        Alert pemergente= new Alert(AlertType.INFORMATION); //tipo de alerta
        pemergente.setTitle("Error al copiar la imagen");
        pemergente.setHeaderText(null);
        pemergente.setContentText("Ha ocurrido un error al copiar, cierre el juego e intentelo luego"); //mensaje
        pemergente.initStyle(StageStyle.UTILITY); //estilo
        pemergente.showAndWait();
    }
    /**
     * Metodo que genera una pantalla emergente cada vez que el usuario trata de ingresar una receta ya existente
     */
    public void pantallaEmergente4(){//ventana salta cuando ya existe una receta
        Alert pemergente= new Alert(AlertType.INFORMATION); //tipo de alerta
        pemergente.setTitle("Recetas");
        pemergente.setHeaderText(null);
        pemergente.setContentText("Esa receta ya existe"); //mensaje
        pemergente.initStyle(StageStyle.UTILITY); //estilo
        pemergente.showAndWait();
    }
    /**
     * Metodo que genera una pantalla emergente cada vez que se guarda correctamente una receta en el archivo que las aloja
     */
    public void pantallaEmergente5(){//ventana salta cuando se guarda correctamente la receta
        Alert pemergente= new Alert(AlertType.INFORMATION); //tipo de alerta
        pemergente.setTitle("Recetas");
        pemergente.setHeaderText(null);
        pemergente.setContentText("Se ha guardado correctamente la receta ingresada!"); //mensaje
        pemergente.initStyle(StageStyle.UTILITY); //estilo
        pemergente.showAndWait();
    }
    /**
     * Metodo que retorna un booleano dependiendo de que si los labels y textField se encuentran vacios o no 
     * @return 
     */
    public boolean  estaLleno(){
        int contador=0;
        for(Node caja:top3.getChildren()){
            VBox contenido=(VBox)caja;
                
                TextField nombre=(TextField)(contenido.getChildren().get(1));
                Label ruta=(Label)(contenido.getChildren().get(2));
                System.out.println(nombre.getText()+"  "+ruta.getText());
                if(nombre.getText().equals("")|| ruta.getText().equals("")){
                    contador+=1;
                }
            
        }
        
        return (contador==0);
    }

    
    
    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    public VBox getTop3() {
        return top3;
    }

    public void setTop3(VBox top3) {
        this.top3 = top3;
    }

    public int getVeces() {
        return veces;
    }

    public void setVeces(int veces) {
        this.veces = veces;
    }
    
    public Label getNombre_platillo() {
        return nombre_platillo;
    }

    public void setNombre_platillo(Label nombre_platillo) {
        this.nombre_platillo = nombre_platillo;
    }

    public TextField getTexto_platillo() {
        return texto_platillo;
    }

    public void setTexto_platillo(TextField texto_platillo) {
        this.texto_platillo = texto_platillo;
    }

    public Label getNum_ingrediente() {
        return num_ingrediente;
    }

    public void setNum_ingrediente(Label num_ingrediente) {
        this.num_ingrediente = num_ingrediente;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
    
    
}
