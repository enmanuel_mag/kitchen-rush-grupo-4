/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;
import java.util.Random;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import Constantes.Constantes;
import static KitchenRush.PanelJuego.gameOver;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class PanelResultados {
    
    private StackPane root;
    private Pane pane;

    private Label njugador1;
    private Label njugador2;
    private Label recetasJ1;
    private Label recetasJ2;
    private Marcador marcador;
    private Jugador jugador1;
    private Jugador jugador2;
    private Label puntajeJ1;
    private Label puntajeJ2;
    private Stage primaryStage;
    private Button nueva;
    private Button menu;
    private MediaPlayer musica;
    private String rutaFondo="/fondoResultados.png";
    
    /**
     * Constructor que recibe un marcador, stage y mediplayer, los inicializa y se llaman a los metodos de la clase.
     * @param marcador
     * @param primaryStage
     * @param musica 
     */
    public PanelResultados(Marcador marcador, Stage primaryStage, MediaPlayer musica){
        //Marcador marcador
        this.marcador=marcador;
        root = new StackPane();
        this.primaryStage = primaryStage;
        pane = new Pane();
        this.musica = musica;
        definirGanador();
        root.setStyle("-fx-background-image: url('"+Constantes.RUTA_IMAGENES+rutaFondo+"');"
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size: "+Constantes.juego_ancho+" "+(Constantes.juego_alto)+"; "
                + "-fx-background-position: center center;");
        root.setAlignment(Pos.CENTER);
 
        
        crearImagenAvatar();
        crearRecetasLogradas();
        crearNombres();
        crearBotones();
        root.getChildren().add(pane);   
    }
    /**
     * Metodo que fija las imagenes de cada uno de los avatares que participaron en el juego en una posicion determinada.
     */
    public void crearImagenAvatar(){
        
        System.out.println(jugador1);
        System.out.println(jugador2);
        pane.getChildren().add(jugador1.getJugador());
        pane.getChildren().add(jugador2.getJugador());
        
        jugador1.fijarPosicionObjeto(110,300);
        jugador2.fijarPosicionObjeto(740,300);
        }
    /**
     * Metodo que fija labels con los nombres y puntajes finales de cada uno de los jugadores participantes.
     */
    public void crearNombres(){
        
        puntajeJ1 = new Label(Integer.toString(jugador1.getPuntos()));
        puntajeJ2 = new Label(Integer.toString(jugador2.getPuntos()));
        
        puntajeJ1.setTextFill(Color.web("#FFFFFF"));
        puntajeJ1.setFont(new Font(28));
        puntajeJ1.setLayoutX(380);
        puntajeJ1.setLayoutY(110);
 
        puntajeJ2.setTextFill(Color.web("#FFFFFF"));
        puntajeJ2.setFont(new Font(28));
        puntajeJ2.setLayoutX(1030);
        puntajeJ2.setLayoutY(110);
        
        pane.getChildren().addAll(puntajeJ1, puntajeJ2);
        
        njugador1=new Label(jugador1.getNombre());
        njugador2=new Label(jugador2.getNombre());
        pane.getChildren().add(njugador1);
        pane.getChildren().add(njugador2);
        
        njugador1.setTextFill(Color.web("#FFFFFF"));
        njugador1.setLayoutX(110);
        njugador1.setLayoutY(370);
        
        njugador2.setTextFill(Color.web("#FFFFFF"));
        njugador2.setLayoutX(740);
        njugador2.setLayoutY(370);
    }
    /**
     * Metodo que fija labels con las recetas cumplidas por cada uno de los jugadores participantes.
     */
    public void crearRecetasLogradas(){
        
        recetasJ1=new Label(jugador1.getEstacion().toString());
        recetasJ2=new Label(jugador2.getEstacion().toString());
        
        recetasJ1.setTextFill(Color.web("#FFFFFF"));
        recetasJ1.setFont(new Font(16));
        
        recetasJ2.setTextFill(Color.web("#FFFFFF"));
        recetasJ2.setFont(new Font(16));
        
        pane.getChildren().add(recetasJ1);
        pane.getChildren().add(recetasJ2);
        
        recetasJ1.setLayoutX(300);
        recetasJ1.setLayoutY(230);
        
        recetasJ2.setLayoutX(930);
        recetasJ2.setLayoutY(230);
    }
/**
 * Metodo que define el ganador del juego usando la clase collections y sus clases estaticas. Setea el fondo del root dependiendo de si hay un ganador o empate. 
 */
    public void definirGanador(){
        
        ArrayList<Jugador> jugadores=new ArrayList<>();
        jugadores.add(marcador.getJugador1());
        jugadores.add(marcador.getJugador2());
        Collections.sort(jugadores);
        Collections.reverse(jugadores);
        jugador1=jugadores.get(0);
        jugador2=jugadores.get(1);
        System.out.println("GANADOR:"+jugador1);
        if (jugador1.getPuntos()==jugador2.getPuntos()){
            rutaFondo="/fondoResultadosEM.png";
        }
        
    }
    /**
     * Metodo que crea los botones de Nueva Partida y MenuPrincipal y los maneja.
     */
    public void crearBotones(){
        
        nueva = new Button("Nueva Partida");
        nueva.setLayoutX(0); nueva.setLayoutY(0);
        
        nueva.setOnMouseClicked(e -> {
        
            Musicas.cancion.stop();
                gameOver=false;

                PanelJuego panelJuego=null;  
                try {
                    panelJuego = new PanelJuego(primaryStage, musica);
                } catch (IOException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PanelInicio.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                primaryStage.getScene().setRoot(panelJuego.getRoot());
        
        });
        
        menu = new Button("Menu Principal");
        
        menu.setOnMouseClicked(e -> {   
            PanelInicio pI = new PanelInicio(primaryStage, false);
            primaryStage.getScene().setRoot(pI.getRoot());
            });
        
        
        menu.setLayoutX(1135); menu.setLayoutY(0);
        
        
        pane.getChildren().addAll(nueva, menu);
        
    }
    
    
    public StackPane getRoot() {
        return root;
    }

    public void setRoot(StackPane root) {
        this.root = root;
    }

    public Pane getPane() {
        return pane;
    }

    public void setPane(Pane pane) {
        this.pane = pane;
    }
    
    
    
}
