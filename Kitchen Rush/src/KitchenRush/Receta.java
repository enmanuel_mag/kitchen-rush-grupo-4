/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KitchenRush;
import Constantes.Constantes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
/**
 *
 * @author Josue
 */
public class Receta implements Serializable{
    
    private String nombre;
    private HashSet<Ingrediente>ingredientes;
    private static int tiempoEspera=30;
    
    /**
     * Constructor de Receta
     * @param nombre
     * @param ingredientes 
     */
    public Receta(String nombre,HashSet<Ingrediente> ingredientes){
        this.nombre=nombre;
        this.ingredientes=ingredientes;
        
    }
    
    
    /**
     * Metodo que valida la equivalenciad de dos objetos Receta, dos recetas son iguales si
     * tienen los mismos ingredientes el mismo nuevo de veces.
     * @return boolean 
     * @param obj
     */
    @Override
    public boolean equals(Object obj){
        int apariciones=0;
        if(obj!=null){
            Receta rc=(Receta)obj;
            if (ingredientes.size()==(rc.getIngredientes().size())){
            for(Ingrediente i:ingredientes){
                if (contador(i)==rc.contador(i)){
                    apariciones++;
                }
            }
        }
        }
        if(apariciones==ingredientes.size()){
                return true;
            }   
        return false;
    }
    
    /**
     * Metodo provicional que recibe un arreglo de Receta y retorna un arreglo de objetos Ingrediente
     * @param recetas
     * @return ingredientes
     */
    public static ArrayList<Ingrediente> ingredientesUnicos(ArrayList<Receta> recetas){
        
        ArrayList<Ingrediente> ingredientes = new ArrayList<>();
        
        for(Receta receta : recetas){
            for(Ingrediente ingrediente: receta.getIngredientes()){ 
                if(!(ingredientes.contains(ingrediente)))
                    ingredientes.add(ingrediente);   
                }
            }
        return ingredientes;    
    }
/**
 * Retorna el nombre de la Receta
 * @return nombre
 */
    public String toString(){
        return nombre;
        
    }
    /**
     * Retorna un set de Ingrediente
     * @return ingredientes
     */
    public HashSet<Ingrediente> getIngredientes() {
        return ingredientes;
    }
/**
 * Actualiza el set Ingrediente de la clase
 * @param ingredientes 
 */
    public void setIngredientes(HashSet<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
    /**
     * Metodo auxiliar que retorna el numero de apariciones de un ingrediente 
     * en la receta que llama al metodo
     * @param ingrediente
     * @return contador
     */
    public int contador(Ingrediente ingrediente){
        int contador=0;
        for(Ingrediente i:ingredientes){
            if(ingrediente.equals(i)){
                contador++;
            }
        }
        return contador;
    }
/**
 * Actualiza el nombre de la receta
 * @param nombre 
 */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
/**
 * Actualiza el tiempo en el que expira un receta
 * @param tiempoEspera 
 */
    public static void setTiempoEspera(int tiempoEspera) {
        Receta.tiempoEspera = tiempoEspera;
    }
/**
 * Retorna el nombre de la receta
 * @return nombre
 */
    public String getNombre() {
        return nombre;
    }
/**
 * Obtiene el tiempo en segundos que le toma a la receta desaparecer
 * @return 
 */
    public static int getTiempoEspera() {
        return tiempoEspera;
    }
    
    
}
