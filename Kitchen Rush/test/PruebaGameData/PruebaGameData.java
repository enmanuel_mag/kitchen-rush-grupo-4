/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PruebaGameData;

import Constantes.Constantes;
import KitchenRush.GameData;
import KitchenRush.Ingrediente;
import KitchenRush.PanelResultados;
import KitchenRush.Receta;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 *
 * @author Josue
 */
public class PruebaGameData extends Application{
    ArrayList<Ingrediente>ingredie=new ArrayList<>();
    ArrayList<Receta>rece=new ArrayList<>();
    VBox root=new VBox();
    Random rd=new Random();
    
    //private PanelResultados panelResultados=new PanelResultados();
    public void set(){
        
         GameData datos=new GameData();
         
         try{
            datos.quemarIngredientes();
            datos.quemarRecetas();
            rece=GameData.cargarRecetas();
            ingredie=GameData.cargarIngredientes();
         }catch(IOException ex){
             System.err.println("Error");
         
         }catch(ClassNotFoundException ex){
             System.err.println("error 2");
         }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        set();
        Pane panel=new Pane();
//        Descomenta para probar datos guardados de ingredientes
        //recorro la lista de ingredientes que se quemaron
        for(Ingrediente i:ingredie){
            //al llamar a i.getObjeto, internamente seteo el imageview a ese ingrediente
            //anado la imageview seteada a un panel tipo pane
            panel.getChildren().add(i.getObjeto());
            //le fijo posiciones aleatoreas
            i.fijarPosicionObjeto(rd.nextDouble()*1080,rd.nextDouble()*720);
        }
        
        //Descomenta para probar datos guardados de receta
        
        for(Receta r:rece){
            System.out.println(r.getNombre());
            for(Ingrediente i2:r.getIngredientes()){
                //i2.setImagen();
                //al llamar a i.getObjeto, internamente seteo el imageview a ese ingrediente
                //anado la imageview seteada a un panel tipo pane
                panel.getChildren().add(i2.getObjeto());
                //le fijo posiciones aleatoreas
                i2.fijarPosicionObjeto(rd.nextDouble()*1080,rd.nextDouble()*720);
            }
        }
        
        root.getChildren().add(panel);
        
        Scene escena= new Scene(root,Constantes.juego_ancho,Constantes.inicio_alto);
        
        primaryStage.setScene(escena);
       
        primaryStage.setTitle("Kitchen Rush - Resultados");
        
        primaryStage.show();
        //To change body of generated methods, choose Tools | Templates.
    }
    
    public static void main(String[] args){
        launch(args);
    }
    
    
}
/*desde aqui, pruebo que funcionen equals , contador y buscarIngrediente
        Ingrediente i1=ingredie.get(0);
        Ingrediente i2=ingredie.get(0) ;
        System.out.println(i1.equals(i2));
        Receta rd0=new Receta("P1",ingredie);
        Receta rd=new Receta("P2",ingredie);
        System.out.println(rd.contador(i1));
        //System.out.println(rd0.equals(rd));
        System.out.println(GameData.buscarIngrediente("choclo").getNombre());
        */