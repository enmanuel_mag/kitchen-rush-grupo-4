/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PruebaPanelResultados;

import Constantes.Constantes;
import KitchenRush.*;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *
 * @author User
 */
public class PruebaPanelResultados extends Application {
    
    private PanelResultados panelResultados;
    
    
    @Override
        public void start(Stage primaryStage){
        
        
        //panelResultados = new PanelResultados();
        
        Scene escena= new Scene(panelResultados.getRoot(),Constantes.juego_ancho,Constantes.inicio_alto);
        
        primaryStage.setScene(escena);
       
        primaryStage.setTitle("Kitchen Rush - Resultados");
        
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
    
}
